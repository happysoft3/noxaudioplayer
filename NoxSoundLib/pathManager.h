
#ifndef PATH_MANAGER_H__
#define PATH_MANAGER_H__

#include "utils\fileStream.h"

class TargetFormat : public FileStream
{
private:
	uint32_t m_uniqueChunk;
	uint32_t m_offset;
	bool m_avaliable;
	enum { ReadCount = 8 };

public:
	explicit TargetFormat(const std::string &targetName, uint32_t offset);
	bool Compare(uint32_t cmp);
private:
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
};

inline bool TargetFormat::Compare(uint32_t cmp)
{
	return m_avaliable ? m_uniqueChunk == cmp : false;
}

class PathManager : public FileStream
{
private:
	std::string m_stream;
	std::string m_path;		//녹스 실행파일 풀 네임이 와야한다!!
	std::string m_pathFilename;
	enum { UniqueFieldKey = 0x2e786f6e, UniqueOffset = 0x187068 };

public:
	explicit PathManager(const std::string &pathFilename);
	std::string CurrentPath();
	bool SetPath(const std::string &path);

private:
	bool AnalysisFormat();
	bool CheckValidContext();
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
	virtual void ReportWriteReady(std::vector<uint8_t> *stream) override;
	bool WritePathFile();
};

#endif