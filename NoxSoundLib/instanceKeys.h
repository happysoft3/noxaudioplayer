
#ifndef INSTANCE_KEYS_H__
#define INSTANCE_KEYS_H__

#include <iostream>

namespace InstanceKeys
{
	enum class InstanceKey
	{
		AUDIO_INDEX = 0,
		AUDIO_KEY,
		WAV_CONTEXT,
		PATH_MANAGER
	};

	namespace TaskID
	{
		constexpr int idAudioIndex = static_cast<int>(InstanceKey::AUDIO_INDEX);
		constexpr int idAudioKey = static_cast<int>(InstanceKey::AUDIO_KEY);
		constexpr int idWavContext = static_cast<int>(InstanceKey::WAV_CONTEXT);
		constexpr int idPathManager = static_cast<int>(InstanceKey::PATH_MANAGER);
	}
}

#endif