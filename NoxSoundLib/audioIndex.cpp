
#include "audioIndex.h"
#include <algorithm>


AudioIndex::AudioIndex()
	: FileStream("audio.idx"), m_streamV(nullptr), m_audioIdxMeta_t(nullptr)
{
	m_streamPos = 0;
}

void AudioIndex::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	m_streamV = fstream;
	if (m_streamV != nullptr)
		LoadData();
}

bool AudioIndex::CheckAudioIdxHeader()
{
	return m_audioIdxMeta_t->header_t == 0x41424147;
}

bool AudioIndex::LoadMetaData()
{
	if (m_streamV->size() > AudioIdxParam::metaDataSize)
	{
		std::vector<uint8_t> cpyRaw(AudioIdxParam::metaDataSize);
		std::copy(m_streamV->begin(), m_streamV->begin() + AudioIdxParam::metaDataSize, cpyRaw.begin());
		m_audioIdxMeta_t = std::make_unique<AudioMeta>(*reinterpret_cast<AudioMeta *>(cpyRaw.data()));
		m_streamPos = AudioIdxParam::metaDataSize;
		return CheckAudioIdxHeader();
	}
	return false;
}

bool AudioIndex::AppendOneOf()
{
	std::vector<uint8_t> cpyField(AudioIdxParam::fieldDataSize);

	CopyStream(m_streamV->begin() + m_streamPos, cpyField.begin(), cpyField.end());
	AudioInfo *audInfo = reinterpret_cast<AudioInfo *>(cpyField.data());
	m_audioInfoM.emplace(audInfo->audioName_t, std::move(*audInfo));
	return true;	//Todo. offset check to bag size
}

void AudioIndex::LoadData()
{
	if (LoadMetaData())
	{
		for (int i = m_audioIdxMeta_t->defCount_t; i; --i)
		{
			if (!(m_streamPos < m_streamV->size()))
				break;
			if (!AppendOneOf())
				break;
			m_streamPos += AudioIdxParam::fieldDataSize;
		}
	}
}

bool AudioIndex::GetAudioData(const std::string &subkey, AudioInfo *dest)
{
	if (m_audioInfoM.size())
	{
		decltype(m_audioInfoM.begin()) &findIt = m_audioInfoM.find(subkey);
		if (findIt != m_audioInfoM.end())
		{
			*dest = findIt->second;
			return true;
		}
	}
	return false;
}