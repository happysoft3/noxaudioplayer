
#ifndef PUBLISHER_H__
#define PUBLISHER_H__

#include "instanceKeys.h"
#include <memory>

class FileStream;
class AudioIndex;
class AudioKey;
class PathManager;
class WavContext;

template <class Module>
class CreatorCtx
{
public:
	template <class... Args>
	std::unique_ptr<FileStream> Create(Args&&... params)
	{
		return std::make_unique<Module>(params...);
	}
};


using namespace InstanceKeys;

class Publisher
{
private:
	template <int N>
	struct DeviceInstance;
	
	template <>
	struct DeviceInstance<TaskID::idAudioIndex>
	{
		template <class... Args>
		std::unique_ptr<FileStream> Get(Args&&... params)
		{
			return CreatorCtx<AudioIndex>().Create(params...);
		}
	};
	template <>
	struct DeviceInstance<TaskID::idAudioKey>
	{
		template <class... Args>
		std::unique_ptr<FileStream> Get(Args&&... params)
		{
			return CreatorCtx<AudioKey>().Create(params...);
		}
	};
	template <>
	struct DeviceInstance<TaskID::idPathManager>
	{
		template <class... Args>
		std::unique_ptr<FileStream> Get(Args&&... params)
		{
			return CreatorCtx<PathManager>().Create(params...);
		}
	};
	template <>
	struct DeviceInstance<TaskID::idWavContext>
	{
		template <class... Args>
		std::unique_ptr<FileStream> Get(Args&&... params)
		{
			return CreatorCtx<WavContext>().Create(params...);
		}
	};
public:
	template <int iKey, class... types>
	std::unique_ptr<FileStream> DoPublish(types&&... params)
	{
		return DeviceInstance<iKey>().Get(params...);
	}

	explicit Publisher();
	~Publisher();

};



#endif