
#ifndef AUDIO_INDEX_H__
#define AUDIO_INDEX_H__

#include "utils/fileStream.h"
#include "audioIndexTypes.h"
#include <iostream>
#include <memory>
#include <map>

/*@brief
* Audio index�� �д� ��ü
**/
class AudioIndex : public FileStream
{
private:
	std::vector<uint8_t> *m_streamV;
	uint32_t m_streamPos;
	std::unique_ptr<AudioMeta> m_audioIdxMeta_t;
	std::map<std::string, AudioInfo> m_audioInfoM;

public:
	explicit AudioIndex();
	AudioIndex(const AudioIndex &) = delete;
	AudioIndex(AudioIndex &&) = delete;
	virtual ~AudioIndex() override { };
private:
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
	bool CheckAudioIdxHeader();
	bool LoadMetaData();
	bool AppendOneOf();
	void LoadData();
	template <class _SrcIt, class _DestIt>
	inline _SrcIt CopyStream(_SrcIt srcIt, _DestIt destBegin, _DestIt destEnd);
public:
	bool GetAudioData(const std::string &subkey, AudioInfo *dest);
};

template <class _SrcIt, class _DestIt>
_SrcIt AudioIndex::CopyStream(_SrcIt srcIt, _DestIt destBegin, _DestIt destEnd)
{
	while (destBegin != destEnd)
	{
		*destBegin = *srcIt;
		++srcIt;
		++destBegin;
	}
	return srcIt;
}

#endif