
#ifndef SOUND_PLUGIN_H__
#define SOUND_PLUGIN_H__


#include "instanceKeys.h"
#include "utils\soundPlayer.h"
#include <string>
#include <memory>
#include <map>
#include <list>


using namespace InstanceKeys;
class Publisher;	//added
class FileStream;
/**
*Todo: PathManager, AudioKey, AudioIndex, WavContext  총 4개 클래스를 팩토리 메서드로 인스턴스 생성을 한다!
*Todo: 이에 따라 각각의 클래스에 대한 팩토리 객체가 하나 씩 더 달라붙어야 할 수도 있다
*/
class SoundPlugin : public SoundPlayer
{
private:
	std::unique_ptr<Publisher> m_publish;
	std::map<InstanceKeys::InstanceKey, std::unique_ptr<FileStream>> m_instance;
	std::string m_sourcePath;
	bool m_state;

	std::string m_playkey;

public:
	explicit SoundPlugin();
	virtual ~SoundPlugin() override;

	virtual bool CheckState() const override;
	virtual bool SendPath(const std::string &path) override;
	virtual bool Initialize() override;
	virtual std::list<std::string> GetAudioKeyList() override;
	virtual std::list<std::string> GetAudioRealnameList(const std::string &key) override;
	virtual void PlayFirst(const std::string &key) override;
	virtual void PlayWithSubkey(const std::string &subkey) override;
	virtual bool ExtractStream(const std::string &subkey, const std::string &output = {}) override;

private:
	void InitPublisher();
	bool InitInstance();
	bool InitializePlugin();

protected:
	virtual bool PlayBefore();
};

#endif