#define MAKEDLL
#include "soundPlayer.h"
#include <fstream>
#include <memory>
#include <functional>

SoundPlayer::SoundPlayer(uint32_t flag)
	: m_playFlag(flag)
{
	m_playSource = "play.wav";
}

SoundPlayer::~SoundPlayer()
{ }

bool SoundPlayer::PlayBefore()
{
	std::unique_ptr < std::ifstream, std::function<void(std::ifstream *cstream)>> playfile(new std::ifstream(m_playSource),
		[](std::ifstream *cstream) { if (cstream->is_open()) cstream->close(); delete cstream; });

	if (playfile->is_open())
	{
		return true;
	}
	return true;
}

void SoundPlayer::PlayAfter()
{
	//
}

void SoundPlayer::Play()
{
	if (PlayBefore())
	{
		std::wstring dest(m_playSource.begin(), m_playSource.end());

		if (dest.length())
		{
			PlaySound(dest.c_str(), nullptr, m_playFlag);

			PlayAfter();
		}
	}
}