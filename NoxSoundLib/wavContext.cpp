
#include "wavContext.h"


WavContext::WavContext()
{
	m_wavHeaderCtx = std::make_unique<WavFormatHeader>();

	m_wavHeaderCtx->header = 0x46464952;
	m_wavHeaderCtx->waveSize = 0;
	m_wavHeaderCtx->waveSymb = 0x45564157;
	m_wavHeaderCtx->fmtSymb = 0x20746d66;
	m_wavHeaderCtx->fmtChunkSize = 0;
	m_wavHeaderCtx->audioFormat = 0x11;
	m_wavHeaderCtx->channels = 0;
	m_wavHeaderCtx->sampleRate = 0;
	m_wavHeaderCtx->byteRate = 88200;
	m_wavHeaderCtx->sampleAlign = 0x400;
	m_wavHeaderCtx->sampleDepth = 0;
	m_wavHeaderCtx->dataSize = 0;
	m_wavHeaderCtx->dataSymb = 0x61746164;	//data symbol
	InitFactField();
}

void WavContext::InitFactField()
{
	m_wavHeaderCtx->factField.alwaysTwo = 2;
	m_wavHeaderCtx->factField.factData = 0x5ea7;
	m_wavHeaderCtx->factField.factLength = 4;
	m_wavHeaderCtx->factField.factSymb = 0x74636166;
}

void WavContext::SetWavHeader()
{
	std::shared_ptr<AudioInfo> audParam = GetAudioParam();

	m_wavHeaderCtx->waveSize = audParam->streamLength + 0x34;
	m_wavHeaderCtx->fmtChunkSize = 0x14;
	m_wavHeaderCtx->dataSize = audParam->streamLength;
	m_wavHeaderCtx->sampleRate = audParam->sampleRate;
	m_wavHeaderCtx->channels = 1;

	if (audParam->sampleRate == 44100)
	{
		m_wavHeaderCtx->factField.unknown1 = 0x7f9;
		m_wavHeaderCtx->byteRate = 0x566d;
		m_wavHeaderCtx->sampleAlign = 0x400;
	}
	else
	{
		m_wavHeaderCtx->factField.unknown1 = 0x3f9;
		if (audParam->sample_align == 0x400)
		{
			m_wavHeaderCtx->channels = 2;
			m_wavHeaderCtx->sampleAlign = 0x400;
			m_wavHeaderCtx->byteRate = 0x56b9;
		}
		else
		{
			m_wavHeaderCtx->byteRate = 0x2b36;
			m_wavHeaderCtx->sampleAlign = 0x200;
		}
	}
	m_wavHeaderCtx->sampleDepth = 4;
}

std::vector<uint8_t> WavContext::GetPrestream()
{
	SetWavHeader();
	std::vector<uint8_t> destV(sizeof(WavFormatHeader));

	std::copy_n(reinterpret_cast<uint8_t *>(m_wavHeaderCtx.get()), destV.size(), destV.begin());
	return destV;
}

std::string WavContext::OutputFileName()
{
	std::shared_ptr<AudioInfo> audParam = GetAudioParam();
	std::string filename;

	filename.reserve(AudioIdxParam::fieldDataSize);
	filename.append(audParam->audioName_t);
	filename.append("_output.wav");
	return filename;
}