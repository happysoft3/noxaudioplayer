
#include "pathManager.h"
#include <algorithm>
#include <iterator>
#include <memory>
#include <functional>

TargetFormat::TargetFormat(const std::string &targetName, uint32_t offset)
	: FileStream(targetName, FileMode::FILE_MODE_READ_RANGE)
{
	m_avaliable = false;
	m_uniqueChunk = 0;
	m_offset = offset;
	SetRange(offset, ReadCount);
}

void TargetFormat::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	if (fstream->size() == ReadCount)
	{
		m_avaliable = true;
		m_uniqueChunk = *reinterpret_cast<uint32_t *>(fstream->data());
	}
}

PathManager::PathManager(const std::string &pathFilename)
	: FileStream(pathFilename)
{
	m_pathFilename = pathFilename;
}

std::string PathManager::CurrentPath()
{
	if (m_path.length())
	{
		uint32_t offset = m_path.find_last_of('\\');
		if (offset)
			return m_path.substr(0, offset);
	}
	return{};
}

bool PathManager::SetPath(const std::string &path)
{
	if (path.length())
	{
		m_stream = path;
		if (CheckValidContext())
		{
			m_path = m_stream;
			return true;
		}
	}
	return false;
}

bool PathManager::AnalysisFormat()
{
	TargetFormat tMat(m_stream, UniqueOffset);

	if (tMat.FileProcess())
		return tMat.Compare(UniqueFieldKey);
	else
		return false;
}

bool PathManager::CheckValidContext()
{
	std::unique_ptr<std::ifstream, std::function<void (std::ifstream *cThis)>> vaildTarget(new std::ifstream(m_stream),
		[](std::ifstream *cThis) { if (cThis->is_open()) cThis->close(); delete cThis; });
	if (vaildTarget->is_open())
	{
		if (AnalysisFormat())
		{
			WritePathFile();

			return true;
		}
	}
	return false;
}

void PathManager::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	std::transform(fstream->begin(), fstream->end(), std::insert_iterator<std::string>(m_stream, m_stream.end()), [](uint8_t chk) { return chk; });
	if (m_stream.length())
	{
		if (CheckValidContext())
		{
			m_path = m_stream;
		}
	}
}

void PathManager::ReportWriteReady(std::vector<uint8_t> *stream)
{
	*stream = std::vector<uint8_t>(m_stream.begin(), m_stream.end());
}

bool PathManager::WritePathFile()
{
	std::unique_ptr<PathManager, std::function<void(PathManager *)>>(this, [this](PathManager *cur) { ChangeFileMode(FileMode::FILE_MODE_READ); });
	ChangeFileMode(FileMode::FILE_MODE_WRITE);

	return FileProcess() > 0;
}

