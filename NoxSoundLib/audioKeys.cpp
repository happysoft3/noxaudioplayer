
#include "audioKeys.h"
#include <algorithm>
#include <iterator>
#include <iostream>

AudioKey::AudioKey(const std::string &fileName)
	: FileStream(fileName), m_keydata("keydata")
{
	m_keyRx = std::regex(R"(\"([^"]*)\")");
	m_valueRx = std::regex(R"(value=([0-9A-z]*))");
}

void AudioKey::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	std::transform(fstream->begin(), fstream->end(), std::insert_iterator<std::string>(m_fstream, m_fstream.begin()), [](uint8_t chk) { return chk; });
	Serialize();
}

void AudioKey::SerializeOneshot(std::string &&src)
{
	std::smatch matched;
	if (src.find_first_of('"') != std::string::npos)
	{
		if (std::regex_search(src, matched, m_keyRx))
			m_keydata = *(++matched.begin());
	}
	if (std::regex_search(src, matched, m_valueRx))
	{
		Append(m_keydata, std::move(*(++matched.begin())));
	}
}

void AudioKey::Serialize()
{
	size_t oldOffset = 0, picker;
	do
	{
		picker = m_fstream.find_first_of('\n', oldOffset);
		SerializeOneshot(m_fstream.substr(oldOffset, picker - oldOffset));
		oldOffset = picker + 1;
	} while (picker != std::string::npos);
}

void AudioKey::Append(const std::string &key, std::string &&value)
{
	decltype(m_preDataM.begin()) keyIterator = m_preDataM.find(key);

	if (keyIterator == m_preDataM.end())
		m_preDataM.emplace(key, std::list<std::string>({ std::forward<std::string>(value) }));
	else
		keyIterator->second.emplace_back(std::forward<std::string>(value));
}

void AudioKey::ShowAll()
{
	std::for_each(m_preDataM.begin(), m_preDataM.end(), [](decltype(*m_preDataM.begin()) mItem)
	{
		std::cout << "Key: " << mItem.first << std::endl;
		std::for_each(mItem.second.begin(), mItem.second.end(), [](std::string value)
		{
			std::cout << '[' << value << "] ";
		});
		std::cout << std::endl;
	});
}

std::list<std::string> AudioKey::FindSound(const std::string &key)
{
	if (m_preDataM.size())
	{
		decltype(m_preDataM.begin()) &fountIt = m_preDataM.find(key);
		if (fountIt != m_preDataM.end())
		{
			return fountIt->second;
		}
	}
	return{};
}

std::string AudioKey::FindSoundFirst(const std::string &key)
{
	std::list<std::string> &findList = FindSound(key);

	if (findList.size())
	{
		return findList.front();
	}
	return std::string();
}

uint32_t AudioKey::GetListCount()
{
	return m_preDataM.size();
}

std::list<std::string> AudioKey::MakeKeyList()
{
	std::list<std::string> keylist;

	std::transform(m_preDataM.begin(), m_preDataM.end(), std::insert_iterator<std::list<std::string>>(keylist, keylist.end()),
		[](decltype(*m_preDataM.begin()) node)->std::string { return node.first; });
	return keylist;
}