
#ifndef AUDIO_KEYS_H__
#define AUDIO_KEYS_H__

#include "utils/fileStream.h"
#include <list>
#include <map>
#include <string>
#include <vector>
#include <regex>

/**
* @brief
* audio_list 를 읽어오는 객체
*/
class AudioKey : public FileStream
{
private:
	std::string m_fstream;
	std::map<std::string, std::list<std::string>> m_preDataM;
	std::string m_keydata;

	std::regex m_keyRx;
	std::regex m_valueRx;

public:
	explicit AudioKey(const std::string &fileName);
	virtual ~AudioKey() override { };

private:
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
	void SerializeOneshot(std::string &&src);
	void Serialize();

public:
	void Append(const std::string &key, std::string &&value);
	void ShowAll();
	std::list<std::string> FindSound(const std::string &key);
	std::string FindSoundFirst(const std::string &key);
	uint32_t GetListCount();
	std::list<std::string> MakeKeyList();
};

#endif