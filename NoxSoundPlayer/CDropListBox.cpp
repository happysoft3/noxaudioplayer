
#include "stdafx.h"
#include "CDropListBox.h"
#include "utils/stringUtils.h"
#include "Resource.h"
#include <algorithm>


CDropListBox::CDropListBox()
	: m_bkColor(RGB(153, 217, 234)), m_bkBrush(RGB(153, 217, 234))
{
}

CDropListBox::~CDropListBox()
{ }

void CDropListBox::ChangeBackgroundColor(UINT color)
{
	m_bkColor = color;
	m_bkBrush.DeleteObject();
	m_bkBrush.CreateSolidBrush(color);
}

void CDropListBox::UpdateItemList(const std::list<std::string> &itemList)
{
	if (itemList.size())
	{
		SetRedraw(false);
		std::for_each(itemList.begin(), itemList.end(), [this](std::string itemname)
		{
			AddString(CString(itemname.c_str()));
		});
		SetCurSel(0);
		SetRedraw(true);
	}
}

void CDropListBox::SetCallback(std::unique_ptr<ListCbType> &&cbDataPtr)
{
	if (m_cbElement == nullptr)
		m_cbElement = std::move(cbDataPtr);
}

HBRUSH CDropListBox::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
	CComboBox::OnCtlColor(pDC, pWnd, nCtlColor);

	pDC->SetBkColor(m_bkColor);
	return m_bkBrush;
}

void CDropListBox::OnSelectedItemChanged()
{
	//리스트 선택 시 이벤트를 여기에서 처리합니다
	CString currentItemName;
	int cursel = GetCurSel();

	GetLBText(cursel, currentItemName);
	if (m_cbElement != nullptr && currentItemName.GetLength())
		std::get<SIGNAL_METHOD>(*m_cbElement)(std::get<LISTENER>(*m_cbElement), currentItemName);
}

BOOL CDropListBox::OnCommand(WPARAM wParam, LPARAM lParam)
{
	OnSelectedItemChanged();
	return CComboBox::OnCommand(wParam, lParam);
}

BEGIN_MESSAGE_MAP(CDropListBox, CComboBox)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()
