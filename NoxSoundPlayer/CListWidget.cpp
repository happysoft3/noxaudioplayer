
#include "stdafx.h"
#include "CListWidget.h"
#include <algorithm>

CListWidget::CListWidget()
{
}

void CListWidget::UpdateListItem(const std::list<std::string> &itemlist)
{
	if (itemlist.size())
	{
		SetRedraw(false);
		DeleteAllItems();
		int column = 0;
		std::for_each(itemlist.begin(), itemlist.end(), [this, &column](std::string itemname)
		{
			InsertItem(column, CString(std::to_string(column).c_str()));
			SetItem(column++, 1, LVIF_TEXT, CString(itemname.c_str()), 0, 0, 0, 0);
		});
		if (itemlist.size())
		{
			EnsureVisible(0, FALSE);
			SetFocus();
			SetItemState(0, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
		}
		SetRedraw(true);
	}
}

CString CListWidget::GetSelectItemString()
{
	CString itemname;
	int selectColumn = GetSelectionMark();
	
	if (selectColumn >= 0)
		return GetItemText(selectColumn, 1);
	return{};
}

void CListWidget::ListInitialize()
{
	InsertColumn(0, L"순번", LVCFMT_LEFT, 50);
	InsertColumn(1, L"데이터", LVCFMT_LEFT, 130);

	ModifyStyle(0, LVS_SINGLESEL, 0);
	SetExtendedStyle(LVS_EX_FULLROWSELECT);
	SetTextColor(RGB(64, 128, 255));
	SetTextBkColor(RGB(200, 191, 231));
	SetBkColor(RGB(200, 191, 231));
}

