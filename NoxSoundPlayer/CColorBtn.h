
#ifndef CCOLOR_BTN_H__
#define CCOLOR_BTN_H__


class CColorBtn : public CButton
{
private:
	void Uint32ToColor(Gdiplus::Color &destColor, UINT32 _color);
	Gdiplus::Color m_clrBkColor;	//배경색상
	Gdiplus::Color m_clrBorderColor;	//내부 테두리 색상
	Gdiplus::Color m_clrInborderColor;	//바깥 테두리
	Gdiplus::Color m_clrTextColor;		//텍스트 색
	Gdiplus::Color m_clrClickTextColor;
	float m_fontSize;
	bool m_btnIsPushed;
	CString m_btnCaption;

public:
	CColorBtn();
	~CColorBtn();

public:
	void SetBkColor(UINT32 _color);
	void SetBorderColor(UINT32 _color);
	void SetTextColor(UINT32 _color);
	void SetFontSize(float _size);
	CString GetButtonCaption() const;
	void ChangeButtonCaption(const CString &caption);
	virtual BOOL Create(LPCTSTR lpszCaption, DWORD _dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID) override;

private:
	void DrawBtnBackground(Gdiplus::Graphics *pg);
	void DrawBtnBorder(Gdiplus::Graphics *pg);
	void DrawBtnText(Gdiplus::Graphics *pg);

protected:
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
};


#endif