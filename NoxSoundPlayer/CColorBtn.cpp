
#include "stdafx.h"
#include "CColorBtn.h"

CColorBtn::CColorBtn()
	: m_fontSize(10.0), m_btnIsPushed(false), m_btnCaption(L"button")
{
	Uint32ToColor(m_clrBkColor, 0xc04020);
	Uint32ToColor(m_clrBorderColor, 0x400000);
	Uint32ToColor(m_clrInborderColor, 0xff7052);
	Uint32ToColor(m_clrTextColor, 0xc8ffff);
	Uint32ToColor(m_clrClickTextColor, 0xff7bbd);
}

CColorBtn::~CColorBtn() {}

void CColorBtn::Uint32ToColor(Gdiplus::Color &destColor, UINT32 _color)
{
	destColor = Gdiplus::Color(0xff, _color & 0xff, (_color >> 0x8) & 0xff, (_color >> 0x10) & 0xff);
}

void CColorBtn::SetBkColor(UINT32 _color)
{
	Uint32ToColor(m_clrBkColor, _color);
}

void CColorBtn::SetBorderColor(UINT32 _color)
{
	Uint32ToColor(m_clrBorderColor, _color);
}

void CColorBtn::SetTextColor(UINT32 _color)
{
	Uint32ToColor(m_clrTextColor, _color);
}

void CColorBtn::SetFontSize(float _size)
{
	m_fontSize = _size;
}

CString CColorBtn::GetButtonCaption() const
{
	return m_btnCaption;
}

void CColorBtn::ChangeButtonCaption(const CString &caption)
{
	m_btnCaption = caption;
}

BOOL CColorBtn::Create(LPCTSTR lpszCaption, DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID)
{
	BOOL blRes = CButton::Create(L"button", dwStyle, rect, pParentWnd, nID);

	m_btnCaption = lpszCaption;
	return blRes;
}

void CColorBtn::DrawBtnBackground(Gdiplus::Graphics *pg)
{
	CRect rect;
	GetClientRect(&rect);

	Gdiplus::SolidBrush brush(m_clrBkColor);
	pg->FillRectangle(&brush, rect.left, rect.top, rect.right, rect.bottom);
}

void CColorBtn::DrawBtnBorder(Gdiplus::Graphics *pg)
{
	CRect rect;
	GetClientRect(&rect);
	Gdiplus::Pen pen(m_clrBorderColor);
	pg->DrawRectangle(&pen, rect.left, rect.top, rect.right - 1, rect.bottom - 1);
	if (!m_btnIsPushed)
	{
		Gdiplus::Pen insidePen(m_clrInborderColor);
		pg->DrawRectangle(&insidePen, rect.left + 1, rect.top + 1, rect.right - 3, rect.bottom - 3);
	}
}

void CColorBtn::DrawBtnText(Gdiplus::Graphics *pg)
{
	CRect rect;

	GetClientRect(&rect);
	float halfXPos = rect.left + static_cast<float>(rect.Width() / 2), halfYPos = rect.top + static_cast<float>(rect.Height() / 2);

	Gdiplus::FontFamily fontFam(L"Arial");
	Gdiplus::Font tFont(&fontFam, m_fontSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	Gdiplus::SolidBrush brush(m_btnIsPushed ? m_clrClickTextColor : m_clrTextColor);
	Gdiplus::StringFormat strForm;

	strForm.SetAlignment(Gdiplus::StringAlignmentCenter);
	strForm.SetLineAlignment(Gdiplus::StringAlignmentCenter);

	pg->DrawString(m_btnCaption, m_btnCaption.GetLength(), &tFont, Gdiplus::RectF(
		static_cast<float>(rect.left), static_cast<float>(rect.top), static_cast<float>(rect.Width()), static_cast<float>(rect.Height())), &strForm, &brush);
}

BEGIN_MESSAGE_MAP(CColorBtn, CButton)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void CColorBtn::OnPaint()
{
	CPaintDC getdc(this);
	CRect rect;

	m_btnIsPushed = (GetState() & BST_PUSHED) != 0;

	GetClientRect(&rect);
	Gdiplus::Graphics mainGrapics(getdc.GetSafeHdc());
	Gdiplus::Bitmap bmp(rect.Width(), rect.Height());
	Gdiplus::Graphics memGrapics(&bmp);
	Gdiplus::SolidBrush brush(m_clrBkColor);
	memGrapics.FillRectangle(&brush, 0, 0, rect.Width(), rect.Height());

	DrawBtnBackground(&memGrapics);
	DrawBtnBorder(&memGrapics);
	DrawBtnText(&memGrapics);

	mainGrapics.DrawImage(&bmp, 0, 0);
}