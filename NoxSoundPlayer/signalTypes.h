
#ifndef SINAL_TYPES_H__
#define SINAL_TYPES_H__

#include <tuple>
#include <functional>

namespace SignalTypes
{
	using SignalType = std::tuple <CDialogEx*, std::function<void(CDialogEx*)>>;
	enum
	{
		SIGNAL_DATA_CTHIS = 0,
		SIGNAL_DATA_SIGNAL
	};
	enum class Signal
	{
		NOTIFY_INITIALIZE,
		NOTIFY_UI_UPDATE,
		NOTIFY_SUBLIST_RELEASE,
		NOTIFY_FAIL_TO_SEARCH
	};
}

#endif